import React, { Component } from 'react';
import ProductCategoryRow from './ProductCategoryRow';
import ProductRow from './ProductRow';
import PropTypes from 'prop-types';

class ProductTable extends Component {
  render() {
    const filterResult = this.props.filterResult;
    // 筛选体育用品类别，并构造 JSX
    const sportsTr = filterResult
      .filter(item => item.category === 'Sporting Goods')
      .map((item, index) => {
        return <ProductRow key={index} name={item.name} price={item.price} />;
      });
    // 筛选电子产品类别，并构造 JSX
    const elecTr = filterResult
      .filter(item => item.category === 'Electronics')
      .map((item, index) => {
        return <ProductRow key={index} name={item.name} price={item.price} />;
      });
    return (
      <table>
        <thead>
          <tr>
            <td>Name</td>
            <td>Price</td>
          </tr>
        </thead>
        <tbody>
          <ProductCategoryRow type="Sporting Goods" />
          { sportsTr }
          <ProductCategoryRow type="Electronics" />
          { elecTr }
        </tbody>
      </table>
    );
  }
}

ProductTable.propTypes = {
  filterResult: PropTypes.arrayOf(PropTypes.object)
};

export default ProductTable;
