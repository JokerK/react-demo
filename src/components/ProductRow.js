import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

class ProductRow extends Component {
  render() {
    return (
      <tr>
        <td>{ this.props.name }</td>
        <td>{ this.props.price }</td>
      </tr>
    );
  }
}

ProductRow.propTypes = {
  name: PropTypes.string,
  price: PropTypes.string
};

export default ProductRow;
