import React, { Component } from 'react';
import PropTypes from 'prop-types';

class SearchBar extends Component {
  render() {
    return (
      <form>
        <input
          type="text"
          placeholder="Search ..."
          value={ this.props.filterText }
          onChange={ this.props.onInputChange.bind(this) } />
        <input
          type="checkbox"
          checked={ this.props.isStockOnly }
          onChange={ this.props.onCheckChange.bind(this) } />
          Only show products in stock.
      </form>
    );
  }
}

SearchBar.propTypes = {
  filterText: PropTypes.string,
  onInputChange: PropTypes.func,
  isStockOnly: PropTypes.bool,
  onCheckChange: PropTypes.func
};

export default SearchBar;
