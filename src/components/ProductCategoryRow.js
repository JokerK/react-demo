import React, { Component } from 'react';
import PropTypes from 'prop-types';

class ProductCategoryRow extends Component {
  render() {
    return (
      <tr>
        <td>{ this.props.type }</td>
      </tr>
    );
  }
}

ProductCategoryRow.propTypes = {
  type: PropTypes.string
};

export default ProductCategoryRow;
