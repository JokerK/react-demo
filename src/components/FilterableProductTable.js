import React, { Component } from 'react';
import SearchBar from './SearchBar';
import ProductTable from './ProductTable';

class FilterableProductTable extends Component {
  constructor (props) {
    super(props);
    this.state = {
      filterText: '',
      isStockOnly: false
    };
    this.data = [
      {category: "Sporting Goods", price: "$49.99", stocked: true, name: "Football"},
      {category: "Sporting Goods", price: "$9.99", stocked: true, name: "Baseball"},
      {category: "Sporting Goods", price: "$29.99", stocked: false, name: "Basketball"},
      {category: "Electronics", price: "$99.99", stocked: true, name: "iPod Touch"},
      {category: "Electronics", price: "$399.99", stocked: false, name: "iPhone 5"},
      {category: "Electronics", price: "$199.99", stocked: true, name: "Nexus 7"}
    ];
  }
  handleInputChange = (evt) => {
    this.setState({
      filterText: evt.target.value
    });
  }
  handleCheckChange = (evt) => {
    this.setState({
      isStockOnly: evt.target.checked
    });
  }
  render() {
    const filterText = this.state.filterText;
    const isStockOnly = this.state.isStockOnly;
    // 根据用户输入字符和是否有货选项筛选数据
    const filterResult = this.data.filter(item => {
      return item.name.toLowerCase().includes(filterText.toLowerCase())
        && (isStockOnly ? item.stocked : true)
    });

    return (
      <div>
        <SearchBar
          filterText={ filterText }
          isStockOnly={ isStockOnly }
          onInputChange={ this.handleInputChange }
          onCheckChange={ this.handleCheckChange } />
        <ProductTable
          filterResult={ filterResult }/>
      </div>
    );
  }
}

export default FilterableProductTable;
